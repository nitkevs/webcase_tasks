"""Tis module contains class Account."""
import pickle
from transaction import Transaction


class Account:
    """Class to store a list of transactions for a specific user.

    Doctests:
    >>> t = Transaction(25, currency='uah')
    >>> a = Account(123, 'my_account')
    >>> a.apply(t)
    >>> print(a, a.balance)
    my_account 25
    """
    def __init__(self, account_number, name='user_account'):
        """
        :param int account_number: Account number.
        :param str name: Account name.
        :param list transaction_list: List of transactions.
        """
        self.__account_number = account_number
        self.name = name
        self.__transaction_list = []

    @property
    def account_number(self):
        """Return account number"""
        return self.__account_number

    @property
    def name(self):
        """Return account name."""
        return self.__name

    @name.setter
    def name(self, value):
        """Check a length of account name while assigning a new value."""
        if len(value) >= 4:
            self.__name = value
        else:
            raise ValueError('Account name cannot be shorter than 4 characters.')

    @property
    def transaction_list(self):
        """Return full list of transactions."""
        return self.__transaction_list

    def __len__(self):
        return len(self.transaction_list)

    def __str__(self):
        return self.name

    def apply(self, transaction: Transaction):
        """Add a transaction to list."""
        self.__transaction_list.append(transaction)

    @property
    def balance(self):
        """Return a current balance."""
        balance = 0
        for transaction in self.transaction_list:
            balance += transaction.amount * transaction.usd_conversion_rate
        return balance

    @property
    def all_usd(self):
        """Return True, if all user transactions are in usd, or False if not."""
        currency_is_usd = map((lambda transaction: transaction.currency == 'usd'), self.__transaction_list)
        return all(currency_is_usd)

    def load(self):
        """Load transactions from a file to transactions list."""
        filename = f'{self.__account_number}.aac'
        with open(filename, 'rb') as file:
            self.__transaction_list = pickle.load(file)

    def save(self):
        """Save transaction list to a file."""
        filename = f'{self.__account_number}.aac'
        with open(filename, 'wb') as file:
            pickle.dump(self.__transaction_list, file)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
