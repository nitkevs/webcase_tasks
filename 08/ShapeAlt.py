"""
This module provides the Point and Circle classes.

>>> point = Point()
>>> point
Point(0, 0)
>>> point.x = 12
>>> str(point)
'(12, 0)'
>>> a = Point(3, 4)
>>> b = Point(3, 4)
>>> a == b
True
>>> a == point
False
>>> a != point
True

>>> circle = Circle(2)
>>> circle
Circle(2, 0, 0)
>>> circle.radius = 3
>>> circle.x = 12
>>> circle
Circle(3, 12, 0)
>>> a = Circle(4, 5, 6)
>>> b = Circle(4, 5, 6)
>>> a == b
True
>>> a == circle
False
>>> a != circle
True
"""

import math


class Point:

    def __init__(self, x=0, y=0):
        """A 2D cartesian coordinate

        >>> point = Point()
        >>> point
        Point(0, 0)
        """
        self.x = x
        self.y = y


    @property
    def distance_from_origin(self):
        """The distance of the point from the origin

        >>> point = Point(3, 4)
        >>> point.distance_from_origin
        5.0
        """
        return math.hypot(self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __add__(self, other):
        """Sum the values of the coordinates of the two points. Return a new Point object.
        >>> q = Point(4, 5)
        >>> r = Point(1, 2)
        >>> q + r
        Point(5, 7)
        """
        return Point(self.x + other.x, self.y + other.y)

    def __iadd__(self, other):
        """Increase the coordinates of one point by the value of the coordinates of another.
        >>> p = Point(4, 5)
        >>> p += Point(1, 2)
        >>> p
        Point(5, 7)
        """
        self.x += other.x
        self.y += other.y
        return self

    def __sub__(self, other):
        """Subtract the coordinates of one point from another. Return a new Point object.
        >>> q = Point(4, 5)
        >>> r = Point(1, 2)
        >>> q - r
        Point(3, 3)
        """
        return Point(self.x - other.x, self.y - other.y)

    def __isub__(self, other):
        """Decrease the coordinates of one point by the value of the coordinates of another.
        >>> p = Point(4, 5)
        >>> p -= Point(1, 2)
        >>> p
        Point(3, 3)
        """
        self.x = self.x - other.x
        self.y = self.y - other.y
        return self

    def __mul__(self, other):
        """Multiply the coordinates of a point by a number. Return a new Point object.
         >>> q = Point(4, 5)
         >>> q * 2
         Point(8, 10)
         """
        return Point(self.x * other, self.y * other)

    def __imul__(self, other):
        """Multiply the coordinates of a point by a number.
        >>> p = Point(4, 5)
        >>> p *= Point(1, 2)
        >>> p
        Point(4, 10)
        """
        self.x *= other
        self.y *= other
        return self

    def __truediv__(self, other):
        """Divide the coordinates of a point by a number. Return a new Point object.
        >>> q = Point(4, 5)
        >>> q / 2
        Point(2.0, 2.5)
        """
        return Point(self.x / other, self.y / other)

    def __itruediv__(self, other):
        """Divide the coordinates of a point by a number.
        >>> q = Point(4, 5)
        >>> q /= 2
        Point(2.0, 2.5)
        """
        self.x /= other
        self.y /= other

    def __floordiv__(self, other):
        """Divide floor the coordinates of a point by a number. Return a new Point object.
        >>> q = Point(4, 5)
        >>> q // 2
        Point(2, 2)
        """
        return Point(self.x // other, self.y // other)

    def __ifloordiv__(self, other):
        """Divide floor the coordinates of a point by a number.
        >>> q = Point(4, 5)
        >>> q //= 2
        Point(2, 2)
        """
        self.x //= other.x
        self.y //= other.y

    def __repr__(self):
        return ("{0.__class__.__name__}({0.x!r}, {0.y!r})".format(
                self))

    def __str__(self):
        return "({0.x!r}, {0.y!r})".format(self)


class Circle(Point):

    def __init__(self, radius, x=0, y=0):
        """A Circle

        >>> circle = Circle(2)
        >>> circle
        Circle(2, 0, 0)
        """
        super().__init__(x, y)
        self.radius = radius


    @property
    def area(self):
        """The circle's area

        >>> circle = Circle(3)
        >>> a = circle.area
        >>> int(a)
        28
        """
        return math.pi * (self.radius ** 2)


    @property
    def edge_distance_from_origin(self):
        """The distance of the circle's edge from the origin

        >>> circle = Circle(2, 3, 4)
        >>> circle.edge_distance_from_origin
        3.0
        """
        return abs(self.distance_from_origin - self.radius)


    @property
    def circumference(self):
        """The circle's circumference

        >>> circle = Circle(3)
        >>> d = circle.circumference
        >>> int(d)
        18
        """
        return 2 * math.pi * self.radius


    @property
    def radius(self):
        """The circle's radius

        >>> circle = Circle(-2)
        Traceback (most recent call last):
        ...
        AssertionError: radius must be nonzero and non-negative
        >>> circle = Circle(4)
        >>> circle.radius = -1
        Traceback (most recent call last):
        ...
        AssertionError: radius must be nonzero and non-negative
        >>> circle.radius = 6
        """
        return self.__radius

    @radius.setter
    def radius(self, radius):
        assert radius > 0, "radius must be nonzero and non-negative"
        self.__radius = radius


    def __eq__(self, other):
        return self.radius == other.radius and super().__eq__(other)


    def __repr__(self):
        return ("{0.__class__.__name__}({0.radius!r}, {0.x!r}, "
                "{0.y!r})".format(self))


if __name__ == "__main__":
    import doctest
    doctest.testmod()
