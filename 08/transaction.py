"""This module contains class Transaction."""
import datetime


class Transaction:
    """This class stores information about a monetary transaction."""
    def __init__(self, amount, currency='usd', usd_conversion_rate=1, description=None):
        """
        :param int amount: Transaction amount.
        :param str currency: Transaction currency.
        :param float usd_conversion_rate: Currency rate to dollar.
        :param str description: Description.

        Additional variables:
        __date: datetime.datetime - Transaction date
        __usd: float - Transaction amount in usd.

        Doctests:
        >>> t = Transaction(25)
        >>> print(t.amount)
        25
        >>> print(t.usd)
        25
        >>> print(t.currency)
        usd
        >>> print(type(t))
        <class 'transaction.Transaction'>
        >>> t.amount = 20
        Traceback (most recent call last):
        ...
        AttributeError: can't set attribute
        """
        self.__amount = amount
        self.__currency = currency
        self.__usd_conversion_rate = usd_conversion_rate
        self.__description = description
        self.__date = datetime.datetime.now()
        self.__usd = amount * usd_conversion_rate

    def __str__(self):
        return f'{self.amount} {self.currency}'

    @property
    def amount(self):
        """Return transaction amount."""
        return self.__amount

    @property
    def currency(self):
        """Return transaction currency."""
        return self.__currency

    @property
    def usd_conversion_rate(self):
        """Return currency rate to dollar"""
        return self.__usd_conversion_rate

    @property
    def description(self):
        """Return description."""
        return self.__description

    @property
    def date(self):
        """Return transaction date."""
        return self.__date

    @property
    def usd(self):
        """Return transaction amount in usd."""
        return self.__usd


if __name__ == "__main__":
    import doctest
    doctest.testmod()
