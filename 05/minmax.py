"""
Functions min and max, for finding, respectively, the minimum and
maximum value of a certain sequence of values.

"""


def min(arg, *args, key=None):
    """
    min (iterable [, key=func]) -> value
    min (arg1, arg2 [, *args, key=func]) -> value

    Return the smallest value among the passed arguments. If the first
    of these is iterable, the minimum is selected from its values.
    Otherwise, at least two values are expected to be passed.
    arg: the first argument or iterable object to compare.
    args: an arbitrary number of elements to compare.
    key: function for sorting arguments.
    """
    if not args:
        items = list(arg)
    else:
        items = [arg]
        items.extend(args)

    items.sort(key=key)
    return items[0]


def max(arg, *args, key=None):
    """
    max (iterable [, key = func]) -> value
    max (arg1, arg2 [, *args, key = func]) -> value

    Return the greatest value among the passed arguments. If the first
    of these is iterable, the maximum is selected from its values.
    Otherwise, at least two values are expected to be passed.
    arg: the first argument or iterable object to compare.
    args: an arbitrary number of elements to compare.
    key: function for sorting arguments.
    """
    if not args:
        items = list(arg)
    else:
        items = [arg]
        items.extend(args)

    items.sort(key=key)
    max_item_index = -1

    # Determine the maximum value found first in the passed sequence
    if key:
        max_returned_value = key(items[-1])
        for i, item in enumerate(items):
            if key(item) == max_returned_value:
                max_item_index = i
                break

    return items[max_item_index]
