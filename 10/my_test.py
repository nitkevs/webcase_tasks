import json
import requests
from bs4 import BeautifulSoup
import lxml

USER_AGENT = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/91.0.4472.114 Safari/537.36 OPR/77.0.4054.146')

response = requests.get('http://books.toscrape.com/catalogue/sharp-objects_997/index.html', headers={'user-agent': USER_AGENT})
response.encoding = 'UTF-8'

print(response.text, response.encoding)

