"""This module contains function wrestler_data."""
import csv
import json

__all__ = ['wrestler_data']


def wrestler_data(wrestlerData: str):
    """
    Read input file, write data from it into json file,
    calculate the statistics of matches and write it to csv and json files,\
    print the statistics on the screen.

    :param str wrestlerData: the name of the input csv file.
    """
    wrestler_list = []
    wrestler_statistics = []

    with open(wrestlerData, 'r') as file:
        reader = csv.reader(file)
        _ = next(reader)
        for row in reader:
            wrestler, wins, losses, draws = row
            wins, losses, draws = map(int, (wins, losses, draws))
            wrestler_list.append({'Wrestler': wrestler, 'Wins': wins, 'Losses': losses, 'Draws': draws, })
            match_statistics = wins + losses + draws
            win_statistics = f'{wins / match_statistics * 100:.1f}%'
            loss_statistics = f'{losses / match_statistics * 100:.1f}%'
            wrestler_statistics.append(
                {'Wrestler': wrestler, 'Matches': match_statistics,
                 'Wins': win_statistics, 'Losses': loss_statistics, }
            )

    file_name_prefix = wrestlerData.rpartition('.')[0]

    with open(f'{file_name_prefix}.json', 'w') as file:
        json.dump(wrestler_list, file, indent=4)

    with open(f'{file_name_prefix}_statistics.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(('Wrestler', 'Matches', 'Wins', 'Losses'))

        headers = ('Wrestler', 'Matches', 'Wins', 'Losses')
        header = ('{0:18} {1:>7} {2:>7} {3:>7}').format(*headers) + \
            '\n------------------+-------+-------+-------'
        print(header)

        for d in wrestler_statistics:
            writer.writerow(d.values())
            print('{0:18.18} {1:>7} {2:>7} {3:>7}'.format(*d.values()))

    with open(f'{file_name_prefix}_statistics.json', 'w') as file:
        json.dump(wrestler_statistics, file, indent=4)
