from django import forms


class ContactForm(forms.Form):
    title = forms.CharField(max_length=20)
    email = forms.EmailField()
    content = forms.CharField(widget=forms.Textarea, max_length=5000)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        print(email)
        if email.endswith('.ru'):
            raise forms.ValidationError('Почта в зоне ru запрещена.')
        return email
