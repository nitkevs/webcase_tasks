"""testblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from .views import (
    homepage,
    aboutpage,
    contactpage
                    )

from blog.views import (
    blogpost_create_view,
    blogpost_delete_view,
    blogpost_detail_view,
    blogpost_list_view,
    blogpost_update_view,
)


urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('', homepage, name='home'),
    path('blog/', include('blog.urls')),
    # path('blog/', blogpost_list_view),
    # path('blog/create/', blogpost_create_view),
    # path('blog/<str:slug>/', blogpost_detail_view),
    # path('blog/<str:slug>/delete/', blogpost_delete_view),
    # path('blog/<str:slug>/update/', blogpost_update_view),
    path('about/', aboutpage, name='about'),
    path('contacts/', contactpage, name='contacts'),
    # path('blog/', blogpost_detail_view, name='blog_post'),
    # path('blog/<int:pk>/', blogpost_detail_view, name='blog_post'),
    # re_path(r'^blog/(?P<pk>\d+)/$', blogpost_detail_view, name='blog_post'),
    # re_path(r'^blog/(?P<slug>\w+)/$', blogpost_detail_view, name='blog_post'),
]
