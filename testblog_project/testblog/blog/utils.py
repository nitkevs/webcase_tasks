def get_slug(post_title):
    slug = []
    for char in post_title:
        if char.isalnum():
            slug.append(char)
        elif char == ' ':
            slug.append('-')
    return ''.join(slug)