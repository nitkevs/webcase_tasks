from django.urls import path

from .views import (
    blogpost_create_view,
    blogpost_delete_view,
    blogpost_detail_view,
    blogpost_list_view,
    blogpost_update_view,
)

urlpatterns = [
    path('', blogpost_list_view, name='blog'),
    path('create/', blogpost_create_view, name='blogpost_create'),
    path('<str:slug>/', blogpost_detail_view, name='blogpost'),
    path('<str:slug>/delete/', blogpost_delete_view, name='blogpost_delete'),
    path('<str:slug>/update/', blogpost_update_view, name='blogpost_update'),
]
