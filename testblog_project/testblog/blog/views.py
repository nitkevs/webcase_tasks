from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django_unique_slugify import unique_slugify
from slugify import slugify

from .forms import BlogPostModelForm
from .models import BlogPost

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

# Create your views here.


def blogpost_detail_view(request, slug):
    blogpost = get_object_or_404(BlogPost, slug=slug)
    context = {'object': blogpost}
    return render(request, template_name='blogpost_detail.html', context=context)


def blogpost_list_view(request):
    qs = BlogPost.objects.all()
    context = {'objects': qs}
    return render(request, template_name='blogpost_list.html', context=context)


@staff_member_required
@login_required
def blogpost_create_view(request):
    """Create a new blog post.

    To generate the slug, two functions of third-party modules are used:
    -- django_unique_slugify.unique_slugify()
    -- slugify.slugify()
    The first creates a unique slug for the model, the second transliterates the Cyrillic alphabet.
    Both modules must be installed in the environment:
    pip3 install django_unique_slugify
    pip3 install python-slugify
    """
    form = BlogPostModelForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=True)
        unique_slugify(obj, slugify(obj.title))
        obj.user = request.user
        obj.save()
        return redirect(obj.get_absolute_url())
    context = {'form': form}
    return render(request, template_name='blogpost_create.html', context=context)


@staff_member_required
@login_required
def blogpost_update_view(request, slug):
    blogpost = get_object_or_404(BlogPost, slug=slug)
    form = BlogPostModelForm(request.POST or None, instance=blogpost)
    if form.is_valid():
        form.save()
        return redirect(f'/blog/{blogpost.slug}/')
    context = {'form': form}
    return render(request, template_name='blogpost_update.html', context=context)


@staff_member_required
@login_required
def blogpost_delete_view(request, slug):
    blogpost = get_object_or_404(BlogPost, slug=slug)
    if request.method == 'POST':
        blogpost.delete()
        return redirect('/blog/')
    context = {'object': blogpost}
    return render(request, template_name='blogpost_delete.html', context=context)
