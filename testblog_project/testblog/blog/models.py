from django.db import models
from django.conf import settings
from django.urls import reverse
# from django.contrib.auth.models import User  # плохая практика

# Create your models here.

User = settings.AUTH_USER_MODEL


class BlogPost(models.Model):
    user = models.ForeignKey(to=User,
                             on_delete=models.SET_NULL,
                             default=1,
                             null=True,)
    title = models.TextField()
    slug = models.SlugField(unique=True)
    content = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'Blog Post - {self.title}'

    def get_absolute_url(self):
        return reverse('blogpost', kwargs={'slug': self.slug})

