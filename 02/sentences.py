import random


articles = ['The', 'A', 'This', 'My', 'Your', 'Her', 'His',
            'Our', 'Another']
nouns = ['bike', 'car', 'ship', 'bus', 'wheel', 'tree',
         'mouse', 'cat', 'dog', 'horse', 'bird', 'sheep',
         'fish', 'phone', 'apple', 'orange', 'melon',
         'peach', 'lemon', 'table', 'chair', 'sofa',
         'pen', 'book', 'pencil', 'house', 'tower',
         'man', 'women', 'boy', 'girl', 'friend']
verbs = ['played', 'danced', 'read', 'spoke', 'looked', 'jumped',
         'laughed', 'rode', 'drank', 'ate', 'cried', 'flew',
         'watched', 'ran', 'sat', 'slept', 'drew', 'screamed']
adverbs = ['ugly', 'well', 'badly', 'proudly', 'funny', 'slowly',
           'fast', 'carefully', 'high', 'low', 'hard', 'often',
           'readily', 'especially', 'accurately']

all_words = [articles, nouns, verbs, adverbs]

for _ in range(5):
    words_quantity = random.randint(3, 4)
    sentence = ''
    words = []
    
    for i in range(words_quantity):
        words.append(random.choice(all_words[i]))
        
    sentence = ' '.join(words)
    print(sentence)
