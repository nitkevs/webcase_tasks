nums = []

while True:
    input_data = input('Enter a number: ')
    try:
        num = int(input_data)
        nums.append(num)
    except ValueError:
        if not input_data:
            break
        else:
            print('Not a number.')
            continue

print('Numbers:', *nums, 'Count:', len(nums),
      'Sum:', sum(nums), 'Minimum:', min(nums),
      'Maximum:', max(nums), 'Average:', sum(nums)/len(nums))
