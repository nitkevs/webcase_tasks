def count_words(string, search_words):
    found_words = set()

    for word in search_words:
        if word in string.lower():
            found_words.add(word)

    return len(found_words)
