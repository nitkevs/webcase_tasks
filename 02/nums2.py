from math import ceil, floor


nums = []

while True:
    input_data = input('Enter a number: ')
    try:
        num = int(input_data)
        nums.append(num)
    except ValueError:
        if not input_data and nums:
            break
        else:
            print('Not a number.')
            continue

print('Numbers:', *nums, 'Count:', len(nums),
      'Sum:', sum(nums), 'Minimum:', min(nums),
      'Maximum:', max(nums), 'Average:', sum(nums)/len(nums), end=' ')

while True:
    replace_count = 0
    
    for i in range(1, len(nums)):
        if nums[i] < nums[i - 1]:
            nums[i], nums[i - 1] = nums[i - 1], nums[i]
            replace_count += 1
    
    if not replace_count:
        break

nums_middle = (len(nums) - 1) / 2
median_min_index = int(floor(nums_middle))
median_max_index = int(ceil(nums_middle))
median = (nums[median_min_index] + nums[median_max_index]) / 2

print('Median:', median)
