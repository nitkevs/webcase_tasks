def sort_by_price(lst):
    return lst['price']


def bigger_price(quantity, all_goods):
    all_goods.sort(key=sort_by_price, reverse=True)
    return all_goods[:quantity]
