def words_order(string, lst):
    words = string.split()
    indexes = []
    result = False

    for word in lst:
        if lst.count(word) > 1:
            break
        try:
            indexes.append(words.index(word))
        except ValueError:
            break

    if len(indexes) == len(lst) and indexes == sorted(indexes):
        result = True

    return result
