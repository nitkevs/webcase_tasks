import math
import sys


filename = input('Input a file name to process: ')
call_lengths = dict()

try:
    with open(filename, 'r', encoding='utf-8') as file:
        for line in file:
            date, time, call_length = line.strip().split()
            call_length = int(call_length)
            minutes = int(math.ceil(call_length / 60))
            call_lengths[date] = call_lengths.get(date, 0) + int(minutes)
except FileNotFoundError:
    print(f'File "{filename}" not found.')
    sys.exit()

total_coast = 0

for length in call_lengths.values():
    if length <= 100:
        total_coast += length
    else:
        total_coast += (length-100) * 2 + 100

print(f'Total coast of calls: {total_coast}')
