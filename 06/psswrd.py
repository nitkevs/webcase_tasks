"""
This module provides functions for generation and validation of passwords
of a given strength.

Functions:
check_passwd -- check the password strength according to the specified parameters.
gen_passwd -- generate a random password according to the specified parameters.
"""
import random
import string

__all__ = ['check_passwd', 'gen_passwd']


def check_passwd(password, length=0, upper=True, punctuation=True, digits=True):
    """
    Check the password according to the specified parameters of a strength.
    Return True, if the password satisfies all the requirements.

    :param str password: The password to validate.
    :param int length: Required password length.
    :param bool upper: Are capital letters required.
    :param bool punctuation: Are punctuation marks required.
    :param bool digits: Are digits required.
    :return: -> bool Does the password meet the specified requirements.
    >>> check_passwd('qwe123sdsd', 8)
    False
    >>> check_passwd('qwe123sdsd', 18)
    False
    >>> check_passwd('qwe123sdsd', 16, False, False, False)
    False
    >>> check_passwd('qwe123sdsd', 10, False, False, True)
    True
    >>> check_passwd('qwe123sdsd', 10, False, True, False)
    False
    >>> check_passwd('qwe123.dsd', 10, False, True, False)
    True
    """
    if len(password) < length:
        return False

    if upper:
        if not any([symbol.isupper() for symbol in password]):
            return False

    if punctuation:
        if not any([(symbol in string.punctuation) for symbol in password]):
            return False

    if digits:
        if not any([symbol.isdigit() for symbol in password]):
            return False

    return True


def gen_passwd(length, upper=True, lower=True, punctuation=True, digits=True):
    """Generate a random password according to the specified parameters.

    The password will contain at least one character from the required character groups
    and will not contain any of those that are equal to False in the passed arguments.

    Notice: If all groups of characters in the passed arguments is equal to False,
    a ValueError exception will be thrown.

    Notice: If the requested password length is less than the number
    of required character groups, a ValueError exception will be thrown.

    :param int length: Password length.
    :param bool upper: Are capital letters required.
    :param bool lower: Are small letters required.
    :param bool punctuation: Are punctuation marks required.
    :param bool digits: Are digits required.
    :return: -> str Password generated according to requirements.
    """
    upper_letters = string.ascii_uppercase
    lower_letters = string.ascii_lowercase
    numeric_characters = string.digits
    punctuation_marks = string.punctuation

    requirements_count = upper + lower + punctuation + digits

    if requirements_count == 0:
        raise ValueError('All passed boolean variables may not be set to False at the same time,\n'
                         'the function need at least one group of characters to generate a password.')

    if length < requirements_count:
        raise ValueError(f'According to the number of requirements for the password strength,\n'
                         f'its length cannot be less than {requirements_count} characters ({length} requested).')

    character_groups = []
    lst = []
    if upper:
        character_groups.append(upper_letters)
        lst.append(random.choice(upper_letters))
    if lower:
        character_groups.append(lower_letters)
        lst.append(random.choice(lower_letters))
    if digits:
        character_groups.append(numeric_characters)
        lst.append(random.choice(numeric_characters))
    if punctuation:
        character_groups.append(punctuation_marks)
        lst.append(random.choice(punctuation_marks))

    while len(lst) < length:
        character_group = random.choice(character_groups)
        symbol = random.choice(character_group)
        lst.append(symbol)

    random.shuffle(lst)
    password = ''.join(lst)
    return password


if __name__ == "__main__":
    import doctest
    doctest.testmod()
