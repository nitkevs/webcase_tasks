"""
This module contains classes for instantiating a lamp
that changes color every time it is turned on.
"""


class GreenColor:
    """Green color for the lamp."""
    def __repr__(self):
        return 'Green'


class RedColor:
    """Red color for the lamp."""
    def __repr__(self):
        return 'Red'


class BlueColor:
    """Blue color for the lamp."""
    def __repr__(self):
        return 'Blue'


class YellowColor:
    """Yellow color of the lamp."""
    def __repr__(self):
        return 'Yellow'


class Lamp:
    """The Lamp object changes its color every time its light method is called.

    Doctests:
    >>> lamp_1 = Lamp()
    >>> lamp_2 = Lamp()
    >>> lamp_1.light()
    'Green'
    >>> lamp_1.light()
    'Red'
    >>> lamp_2.light()
    'Green'
    >>> lamp_1.light() == "Blue"
    True
    >>> lamp_1.light() == "Yellow"
    True
    >>> lamp_1.light() == "Green"
    True
    >>> lamp_2.light() == "Red"
    True
    >>> lamp_2.light() == "Blue"
    True
    """
    def __init__(self):
        """
        Variables:
        self._color: Color of the lamp. Initial color is green.
        self.colors: Generator returns four colors in the loop.
        """
        self._color = GreenColor()
        self.colors = self.color_generator()

    def change_color(self) -> None:
        """Change current color of a lamp."""
        self._color = next(self.colors)

    def light(self):
        """Return current color of a lamp and change it to the next."""
        self.change_color()
        return str(self._color)

    @staticmethod
    def color_generator():
        """Return a generator object for changing colors."""
        while True:
            yield GreenColor()
            yield RedColor()
            yield BlueColor()
            yield YellowColor()


if __name__ == '__main__':
    import doctest
    doctest.testmod()
