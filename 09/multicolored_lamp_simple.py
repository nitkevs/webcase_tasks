"""This module contains class Lamp."""


class Lamp:
    """The Lamp object changes its color every time its light method is called.

    Doctests:
    >>> lamp_1 = Lamp()
    >>> lamp_2 = Lamp()
    >>> lamp_1.light()
    'Green'
    >>> lamp_1.light()
    'Red'
    >>> lamp_2.light()
    'Green'
    >>> lamp_1.light() == "Blue"
    True
    >>> lamp_1.light() == "Yellow"
    True
    >>> lamp_1.light() == "Green"
    True
    >>> lamp_2.light() == "Red"
    True
    >>> lamp_2.light() == "Blue"
    True
    """
    def __init__(self):
        """
        self.color is a generator that returns the name of the color (Green, Red, Blue, Yellow)
        in an infinite loop.
        """
        self.colors = self.color_generator()

    def light(self):
        """
        Return the next color name.
        """
        color = next(self.colors)
        return color

    @staticmethod
    def color_generator():
        """Return a generator object for changing colors."""
        while True:
            yield 'Green'
            yield 'Red'
            yield 'Blue'
            yield 'Yellow'


if __name__ == '__main__':
    import doctest
    doctest.testmod()
