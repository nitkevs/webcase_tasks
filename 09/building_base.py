"""This module contains class Building, for the instantiating a building."""


class Building:
    def __init__(self, south, west, width_we, width_ns, height=10):
        """
        :param numeric south: The position of the southernmost point of the building.
        :param numeric west: The position of the westernmost point of the building.
        :param numeric width_we: The length of the southern wall of the building.
        :param numeric width_ns: The length of the western wall of the building.
        :param numeric height: The height of the building.

        Additional variables:
        self.north: numeric, the position of the nothernmost point of the building.
        self.east: numeric, the position of the easternmost point of the building.

        Doctests:
        >>> Building(10, 10, 1, 2, 2)
        "Building(10, 10, 1, 2, 2)"
        >>> Building(0, 0, 10.5, 2.546)
        "Building(0, 0, 10.5, 2.546, 10)"
        """
        self.south = south
        self.west = west
        self.width_we = width_we
        self.width_ns = width_ns
        self.height = height
        self.north = south + width_ns
        self.east = west + width_we

    def corners(self):
        """
        Return the coordinates of the points for the northwest,
        northeast, southwest, and southeast corners of the building, respectively.

        Variables:
        corners: dict, coordinates of the corners of the building.

        >>> Building(1, 2, 2, 2).corners()
        {'north-west': (3, 2), 'north-east': (3, 4), 'south-west': (1, 2), 'south-east': (1, 4)}
        """
        corners = {
            'north-west': (self.north, self.west),
            'north-east': (self.north, self.east),
            'south-west': (self.south, self.west),
            'south-east': (self.south, self.east),
        }
        return corners

    def area(self):
        """Return the building area.

        >>> Building(1, 2.5, 4.2, 1.25).area()
        5.25
        """
        return self.width_ns * self.width_we

    def volume(self):
        """Return the building volume.

        >>> Building(1, 2.5, 4.2, 1.25, 101).volume()
        530.25
        """
        return self.area() * self.height

    def __repr__(self):
        """
        >>> str(Building(0, 0, 10.5, 2.546))
        '"Building(0, 0, 10.5, 2.546, 10)"'
        """
        return f'"Building({self.south}, {self.west}, {self.width_we}, {self.width_ns}, {self.height})"'


if __name__ == '__main__':
    import doctest
    doctest.testmod()
