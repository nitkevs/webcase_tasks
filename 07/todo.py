"""The application allows to create and edit to-do list in interactive mode."""
import datetime
import sys


class Task:
    """The class describes the properties of the user's task instance."""
    def __init__(self, title='', is_done=False, description=''):
        self.title = title
        self.is_done = is_done
        self.description = description
        self.date = datetime.datetime.now()


class TodoList:
    """The class describes an instance of the user's to-do list."""
    def __init__(self):
        self.filename = 'todo.txt'
        self.task_list = self._get_list_from_file()

    def add(self):
        """Add a task in to-do list."""
        while True:
            title = input('Enter a task title: ').strip()
            if not title:
                continue
            else:
                break

        task = Task(title)
        task.description = input('Enter a task description: ')
        self.task_list.append(task)
        self._save()

    def view(self, num):
        """Show detailed properties of a task with specified number such as title, creation date, etc."""
        task = self.task_list[num - 1]
        print(f'\nTask {num}:\n{task.title}\n{task.description}\n'
              f'Created: {task.date.strftime("%Y-%m-%d %H:%M:%S")}\n'
              f'{"completed" if task.is_done else "not completed"}\n')

    def edit(self, num):
        """Let the user edit the task."""
        print('\nFill in the fields you want to edit.')
        task = self.task_list[num - 1]
        title = input('Enter a task title: ').strip()
        task.title = title or task.title
        description = input('Enter a task description: ')
        task.description = description or task.description
        while True:
            is_done = input('Enter "1" if task is completed, or "0" if not: ')
            if is_done != '':
                values = {'0': False, '1': True}
                try:
                    task.is_done = values[is_done]
                    break
                except KeyError:
                    continue
            break
        self._save()

    def delete(self, num):
        """Delete task with the specified number."""
        del self.task_list[num - 1]
        self._save()

    def done(self, num):
        """Mark task as completed."""
        self.task_list[num - 1].is_done = True
        self._save()

    def list(self):
        """Show the full to-do list."""
        print('\nYour tasks:')
        for num, task in enumerate(self.task_list, 1):
            print(f'{num}. {task.title[:15]:<16} {"completed" if task.is_done else "not completed"}')
        print()

    def _save(self):
        """The method used by other class methods to save the current state of the to-do list"""
        with open(self.filename, 'w') as file:
            for task in self.task_list:
                file.write(f'{task.title}\n'
                           f'{task.description}\n'
                           f'{task.date.strftime("%Y-%m-%d %H:%M:%S")}\n'
                           f'{str(task.is_done)}\n<-->\n')

    def _get_list_from_file(self):
        """Get a to-do list from a file or create an empty one. Used by __init__ method."""
        try:
            self.filename = sys.argv[1]
        except IndexError:
            pass

        try:
            with open(self.filename, 'r') as file:
                file_content = file.read().rstrip('<->\n')
        except FileNotFoundError:
            with open(self.filename, 'w'):
                file_content = ''

        task_list = file_content.split('\n<-->\n') if file_content else []
        parsed_task_list = []

        for item in task_list:
            task = Task()
            fields = item.split('\n')
            title, description, date, is_done = fields
            task.title = title
            task.description = description
            task.date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            task.is_done = eval(is_done)
            parsed_task_list.append(task)

        return parsed_task_list


def main():
    """Run the app."""
    todo_list = TodoList()

    while True:
        print('Command list:\nadd - add new task\nview <number> - view task number <number>\n'
              'edit <number> - edit task number <number>\ndelete <number> - delete task number <number>\n'
              'done <number> - mark task number <number> as done\nlist - get task list\nexit - exit')
        commands = {'add': todo_list.add, 'view': todo_list.view,
                    'edit': todo_list.edit, 'delete': todo_list.delete,
                    'done': todo_list.done, 'list': todo_list.list,
                    'exit': sys.exit}
        command_args = input('Enter a command: ').split()

        try:
            if len(command_args) > 1:
                commands[command_args[0]](int(command_args[1]))
            else:
                commands[command_args[0]]()
        except TypeError:
            print(f'\nERROR\nCommand "{command_args[0]}" missing number of a task.\n')
            continue
        except KeyError:
            print(f'\nERROR\nUnknown command "{command_args[0]}"\n')
        except (IndexError, ValueError):
            if command_args:
                print('\nERROR\nTask not found.\n')
            else:
                continue


main()
