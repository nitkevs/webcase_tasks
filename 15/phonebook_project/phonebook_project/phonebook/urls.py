from django.urls import path
from . import views

urlpatterns = [
    path('', views.subscriber_list_view, name='index'),
    path('subscriber/<int:pk>', views.subscriber_detail_view, name='subscriber_detail'),
]
