from django.urls import path
from . import views

urlpatterns = [
    path('', views.subscriber_list_view, name='index'),
    path('subscriber/<int:pk>', views.subscriber_detail_view, name='subscriber_detail'),
    path('subscriber/create/', views.subscriber_create_view, name='subscriber_create'),
    path('subscriber/<int:pk>/update/', views.subscriber_update_view, name='subscriber_update'),
    path('subscriber/<int:pk>/delete/', views.subscriber_delete_view, name='subscriber_delete'),
]
