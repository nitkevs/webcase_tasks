from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from .forms import SubscriberCreateForm, SubscriberUpdateForm
from .models import Subscriber
from

def subscriber_list_view(request):
    """Return a subscriber list page."""
    title = 'Телефонная книга'
    subscriber_list = Subscriber.objects.all()
    return render(request, 'phonebook/index.html', context={'title': title, 'subscriber_list': subscriber_list})


def subscriber_detail_view(request, pk):
    """Return a subscriber detail page."""
    subscriber = get_object_or_404(Subscriber, id=pk)
    return render(request, 'phonebook/subscriber.html', context={'subscriber': subscriber})


def subscriber_create_view(request):
    """Return a subscriber create page."""
    form = SubscriberCreateForm(request.POST or None)
    if form.is_valid():
        subscriber = Subscriber(**form.cleaned_data)

        subscriber.save()
        subscriber_detail_url = reverse("subscriber_detail", args=[subscriber.id])
        messages.success(request, f'Абонент <a href="{subscriber_detail_url}">{subscriber}</a> успешно создан.')
        return HttpResponseRedirect(reverse('subscriber_detail', args=[subscriber.id]))
    return render(request, 'phonebook/subscriber_create.html', context={'form': form})


def subscriber_update_view(request, pk):
    """Return a subscriber edit page."""
    subscriber = get_object_or_404(Subscriber, pk=pk)
    if request.method == 'POST':
        form = SubscriberUpdateForm(request.POST, instance=subscriber)
        if form.is_valid():
            form.save()
            subscriber_detail_url = reverse("subscriber_detail", args=[subscriber.id])
            messages.success(request, f'Абонент <a href="{subscriber_detail_url}">{subscriber}</a> успешно обновлён.')
            return HttpResponseRedirect(reverse('subscriber_detail', args=[subscriber.id]))
    else:
        form = SubscriberUpdateForm(instance=subscriber)
    return render(request, 'phonebook/subscriber_update.html', context={'form': form, 'subscriber': subscriber})


def subscriber_delete_view(request, pk):
    """Return a subscriber delete page."""
    subscriber = get_object_or_404(Subscriber, pk=pk)
    if request.method == 'POST':
        subscriber.delete()
        messages.success(request, f'Абонент {subscriber} успешно удалён.')
        return HttpResponseRedirect('/')
    return render(request, 'phonebook/subscriber_delete.html', {'subscriber': subscriber})
