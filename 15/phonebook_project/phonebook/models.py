from django.db import models
from django.urls import reverse

# Create your models here.


class Subscriber(models.Model):
    """An instance of a subscriber."""
    first_name = models.CharField(max_length=50, verbose_name='Имя', blank=True, null=True)
    last_name = models.CharField(max_length=50, verbose_name='Фамилия', blank=True, null=True)
    phone_number = models.CharField(max_length=20, verbose_name='Телефон')
    birth_date = models.DateField(verbose_name='Дата рождения', blank=True, null=True)
    comment = models.TextField(verbose_name='Комментарий', blank=True, null=True)

    def __str__(self):
        """Full name of the subscriber consists of his last_name and/or his first_name,
        if first_name and last_name fields are not filled full name is 'Неизвестный абонент'
        """
        if not self.last_name and not self.first_name:
            full_name = 'Неизвестный абонент'
        else:
            full_name = f'{self.last_name or ""} {self.first_name or ""}'.strip()
        return full_name

    def get_absolute_url(self):
        return reverse('subscriber_detail', kwargs={'pk': self.pk})
