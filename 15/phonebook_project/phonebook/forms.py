from django import forms

from .models import Subscriber


class SubscriberCreateForm(forms.Form):
    first_name = forms.CharField(required=False, label="Имя")
    last_name = forms.CharField(required=False, label="Фамилия")
    phone_number = forms.CharField(label="Телефон")
    birth_date = forms.DateField(required=False, label="Дата рождения")
    comment = forms.CharField(widget=forms.Textarea(attrs={'cols': '50', 'rows': '12'}), required=False, label="Комментарий")


class SubscriberUpdateForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['first_name', 'last_name', 'phone_number', 'birth_date', 'comment']
