def index_power(input_list, num):
    try:
        output_number = input_list[num] ** num
    except IndexError:
        output_number = -1
    return output_number
