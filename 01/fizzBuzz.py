def fizzBuzz(num):
    lst = []
    if num % 3 == 0:
        lst.append('Fizz')
    if num % 5 == 0:
        lst.append('Buzz')
    if not lst:
        lst.append(str(num))
    output = ' '.join(lst)
    return output
