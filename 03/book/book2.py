import string


with open('book.txt', 'r', encoding='UTF-8') as book:
    text = book.read()
    non_letters = string.punctuation + '—…«»'
    letter_quantity = 0
    text_words = ''
    punctuation_marks_quantity = 0

    for symbol in text:
        if symbol.isalpha():
            letter_quantity += 1

        if symbol not in non_letters:
            text_words += symbol
        else:
            punctuation_marks_quantity += 1

    words_list = text_words.split()

    for i in range(len(words_list)):
        words_list[i] = words_list[i].lower()

    words_quantity = len(words_list)
    words_set = set(words_list)
    unique_words_quantity = len(words_set)

with open('book.txt', 'r', encoding='UTF-8') as book:
    lines = book.readlines()
    non_empty_strings = [line for line in lines if line.strip()]
    non_double_empty_strigs = ''

    text = ''
    for line in lines:
        text += line.strip(' ')

    text = text.replace('\n\n\n', '\n\n')

with open('book.txt', 'w', encoding='UTF-8') as file:
    file.write(text)

headers_quantity = text.count('\nРОЗДІЛ')
print(headers_quantity)

print('Total letters quantity:', letter_quantity, 'Total words quantity:', words_quantity, 'Unique words quantity:',
      unique_words_quantity, 'Strings with text quantity:', len(non_empty_strings), 'Total punctuation marks quantity:',
      punctuation_marks_quantity, 'Total headers quantity:', headers_quantity, sep='\n')
