import string


def to_encrypt(string_to_encrypt, shift):
    output = ''
    alphabet = string.ascii_lowercase

    for letter in string_to_encrypt:
        try:
            index = alphabet.index(letter)
            shifted_index = index + shift
            shifted_index %= len(alphabet)
            output += alphabet[shifted_index]
        except ValueError:
            output += ' '

    return output
