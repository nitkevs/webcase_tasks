import calendar


def date_time(input_string):
    date_string = ''
    for symbol in input_string:
        if symbol.isdigit():
            date_string += symbol
        else:
            date_string += ' '

    day, month, year, hours, minutes = [int(s) for s in date_string.split()]
    month = calendar.month_name[month]

    if hours == 1:
        h = 'hour'
    else:
        h = 'hours'

    if minutes == 1:
        m = 'minute'
    else:
        m = 'minutes'

    return f'{day} {month} {year} year {hours} {h} {minutes} {m}'
