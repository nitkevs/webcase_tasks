import string


def to_decrypt(string_to_decrypt, shift):
    output = ''
    alphabet = string.ascii_lowercase
    allowed_characters = alphabet + ' '

    for letter in string_to_decrypt:
        if letter in allowed_characters:
            try:
                index = alphabet.index(letter)
                shifted_index = index + shift
                shifted_index %= len(alphabet)
                output += alphabet[shifted_index]
            except ValueError:
                output += ' '

    return output
