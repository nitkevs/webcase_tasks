"""
This module contains the classes Warrior and Knight for creating warrior objects.
And it also contains the function fight, which implements a duel between two warriors.

>>> chuck = Warrior()
>>> bruce = Warrior()
>>> carl = Knight()
>>> dave = Warrior()
>>> fight(chuck, bruce)
True
>>> fight(dave, carl)
False
>>> chuck.is_alive
True
>>> bruce.is_alive
False
>>> carl.is_alive
True
>>> dave.is_alive
False
"""

__all__ = ['Warrior', 'Knight', 'fight']


class Warrior:
    """The warrior instance."""
    def __init__(self):
        """
        Variables:
        self.attack (int) -- The power of a warrior's blow.
        self.health (int) -- Warrior health.
        """
        self.attack = 5
        self.health = 50

    @property
    def is_alive(self):
        """
        Return is the warrior's health value greater then 0.
        A value of 0, or less means the warrior is dead.
        """
        return self.health > 0 or False


class Knight(Warrior):
    """The knight warrior instance."""
    def __init__(self):
        """
        Variables:
        self.attack (int) -- The power of a warrior's blow.
        """
        super().__init__()
        self.attack = 7


def fight(first_warrior, second_warrior):
    """Fight the warriors, return True if the first warrior wins, or False if the second one."""
    attacker = first_warrior
    attacked = second_warrior
    while attacker.is_alive:
        attacked.health -= attacker.attack
        attacker, attacked = attacked, attacker
    return first_warrior.is_alive or False


if __name__ == '__main__':
    import doctest
    doctest.testmod()
