import json
import os
import urllib.parse
import urllib.request
import urllib.error

from bs4 import BeautifulSoup
import lxml
import requests


class Book:
    def __init__(self, url):
        self.USER_AGENT = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/91.0.4472.114 Safari/537.36 OPR/77.0.4054.146')
        request = urllib.request.Request(url, headers={"User-Agent": self.USER_AGENT})
        response = urllib.request.urlopen(request).read().decode('utf-8')
        page_html = BeautifulSoup(response, 'lxml')

        self.title = page_html.h1.text
        self.price = page_html.find('p', {'class': ['price_color']}).text[1:]
        image_link = page_html.find('div', {'id': ['product_gallery']}).img.get('src')
        self.image_url = urllib.parse.urljoin(url, image_link)
        self.image_name = self.get_image_name(self.title, self.image_url)
        self.availability = page_html.find('p', {'class': ['availability']}).text.strip()
        self.rating = self.get_rating(page_html)
        self.description = page_html.find('div', {'id': ['product_description']}).find_next_sibling().text
        self.upc = page_html.find('th', text='UPC').find_next_sibling().text

    @staticmethod
    def get_image_name(title, url):
        image_name = ''
        for symbol in title.lower():
            if symbol.isalnum():
                image_name += symbol
            elif symbol == ' ':
                image_name += '_'

        image_extension = url.rpartition('.')[2]
        image_name += f'.{image_extension}'
        return image_name

    @staticmethod
    def get_rating(page_html):
        ratings = {'One': 1, 'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5}
        rating_key = page_html.find('p', {'class': ['star-rating']}).get('class')[1]
        return ratings[rating_key]

    def get_data(self):
        data = dict()
        data['title'] = self.title
        data['price'] = self.price
        data['image_name'] = self.image_name
        data['availability'] = self.availability
        data['description'] = self.description
        data['upc'] = self.upc
        return data


class BooksPage:
    def __init__(self, page_number):
        self.USER_AGENT = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
           'Chrome/91.0.4472.114 Safari/537.36 OPR/77.0.4054.146')
        self.page_number = page_number

    def links(self):  # !!!
        links = []
        url = f'https://books.toscrape.com/catalogue/category/books/mystery_3/page-{self.page_number}.html'
        request = urllib.request.Request(url, headers={"User-Agent": self.USER_AGENT})

        try:
            response = urllib.request.urlopen(request).read().decode('utf-8')
        except urllib.error.HTTPError:
            response = ''

        page_html = BeautifulSoup(response, 'lxml')
        divs = page_html.find_all('div', {'class': ['image_container']})
        page_links = [link.a.get('href') for link in divs]
        links.extend(page_links)
        return links

    def get_books_info(self):
        data = []
        links = self.links()
        for link in links:
            url = urllib.parse.urljoin('https://books.toscrape.com/catalogue/category/books/mystery_3/', link)
            book = Book(url)
            data.append(book.get_data())
            self.download_book_image(book)
        return data

    @staticmethod
    def download_book_image(book):
        response = requests.get(book.image_url)
        while True:
            try:
                with open(f'images/{book.image_name}', 'wb') as file:
                    file.write(response.content)
                break
            except NotADirectoryError as e:
                e.strerror = ('Cannot create the "images" directory, a file with the same name already exists.\n'
                    'Delete or rename it and run the script again.\nCannot write file')
                raise e
            except FileNotFoundError:
                os.mkdir('images')
                continue


class Json:
    @staticmethod
    def books_info():
        data_for_json = []
        page_number = 1
        while True:
            page = BooksPage(page_number)
            data = page.get_books_info()
            if data:
                data_for_json.extend(data)
                page_number += 1
            else:
                break
        return data_for_json

    def write_file(self):
        with open('books.json', 'w', encoding='UTF-8') as file:
            json.dump(self.books_info(), file, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    j = Json()
    j.write_file()
