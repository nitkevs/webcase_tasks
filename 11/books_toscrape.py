"""
This application downloads books information from 'https://books.toscrape.com'
from the 'Mystery' section and saves it in the 'books.json' file in the same directory
as the script file. Book covers are also downloaded to the 'images' subdirectory.

Classes:
Book -- a book instance.
BooksPage -- an instance of a book catalog page.
Json -- the main class that uses the previous two to get information about books and save it to a file.

Usage:
Just run application.

Usage with import:
- Import class Json, create its instance and call its write_file() method
(see the example at the end of the script).
"""
import json
import os
import urllib.parse

from bs4 import BeautifulSoup
import lxml
import requests

__all__ = ['Json']


class Book:
    """Stores information about book instance.

    Purpose:
    Download a page about a book, extract the necessary information, return it in a dict object.
    """
    def __init__(self, url):
        """
        :param str url: book page url.
        """
        self.USER_AGENT = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                           'Chrome/91.0.4472.114 Safari/537.36 OPR/77.0.4054.146')
        response = requests.get(url, headers={'user-agent': self.USER_AGENT})
        response.encoding = 'UTF-8'
        page_html = BeautifulSoup(response.text, 'lxml')

        self.title = page_html.h1.text
        self.price = page_html.find('p', {'class': ['price_color']}).text[1:]
        image_link = page_html.find('div', {'id': ['product_gallery']}).img.get('src')
        self.image_url = urllib.parse.urljoin(url, image_link)
        self.image_name = self.get_image_name(self.title, self.image_url)
        self.availability = page_html.find('p', {'class': ['availability']}).text.strip()
        self.rating = self.get_rating(page_html)
        self.description = page_html.find('div', {'id': ['product_description']}).find_next_sibling().text
        self.upc = page_html.find('th', text='UPC').find_next_sibling().text

    @staticmethod
    def get_image_name(title, url):
        """
        Generate the name of the image file using the book name
        and the extension of the original file.

        :param str title: the book name.
        :param str url: url of the cover file.
        :return: -> str generated name of the image file.
        """
        image_name = ''
        for symbol in title.lower():
            if symbol.isalnum():
                image_name += symbol
            elif symbol == ' ':
                image_name += '_'

        image_extension = url.rpartition('.')[2]
        image_name += f'.{image_extension}'
        return image_name

    @staticmethod
    def get_rating(page_html):
        """
        Return a book rating.

        :param BeautifulSoup page_html: BeautifulSoup object containing full html code of a book page.
        :return: -> int a book rating.
        """
        ratings = {'One': 1, 'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5}
        rating_key = page_html.find('p', {'class': ['star-rating']}).get('class')[1]
        return ratings[rating_key]

    def get_data(self):
        """Return a dict object containing book information."""
        data = dict()
        data['title'] = self.title
        data['price'] = self.price
        data['image_name'] = self.image_name
        data['availability'] = self.availability
        data['description'] = self.description
        data['upc'] = self.upc
        return data


class BooksPage:
    """An instance of the book catalog page.

    Purpose:
    Download a book catalog page, collect all book page links from it,
    request information from instances of the books,
    download and save books cover image,
    return list object with a dict for each book.
    """
    def __init__(self, page_number):
        self.USER_AGENT = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                           'Chrome/91.0.4472.114 Safari/537.36 OPR/77.0.4054.146')
        self.page_number = page_number

    def links(self):
        """Return list of links to book pages found on the catalog page."""
        links = []
        url = f'https://books.toscrape.com/catalogue/category/books/mystery_3/page-{self.page_number}.html'
        response = requests.get(url, headers={'user-agent': self.USER_AGENT})
        response.encoding = 'UTF-8'
        page_html = BeautifulSoup(response.text, 'lxml')
        image_containers = page_html.find_all('div', {'class': ['image_container']})
        links_list = [link.a.get('href') for link in image_containers]
        links.extend(links_list)
        return links

    def books_info(self):
        """
        Download all book pages from current catalog page,
        request information from instances of the books,
        return list object with a dict with each book info.
        """
        data = []
        links = self.links()
        for link in links:
            url = urllib.parse.urljoin('https://books.toscrape.com/catalogue/category/books/mystery_3/', link)
            book = Book(url)
            data.append(book.get_data())
            self.download_book_image(book)
        return data

    @staticmethod
    def download_book_image(book):
        """Download a book cover image in the 'images' directory. Create a directory if not exists.

        :param Book book: a book instance.

        Notice: A NotADirectoryError exception will be thrown, when file 'images' is already exists,
        and it's not a directory. You must rename, remove, or delete this file an run app once again.
        """
        response = requests.get(book.image_url)
        while True:
            try:
                with open(f'images/{book.image_name}', 'wb') as file:
                    file.write(response.content)
                break
            except NotADirectoryError as e:
                e.strerror = ('Cannot create the "images" directory, a file with the same name already exists.\n'
                              'Delete or rename it and run the script again.\nCannot write file')
                raise e
            except FileNotFoundError:
                os.mkdir('images')
                continue


class Json:
    """
    Main class of application.

    Purpose:
    Request books information from a BooksPage instances,
    write it to a json file.
    """
    @staticmethod
    def books_info():
        """
        Request books information from a BooksPage instances.
        Return a list object with a dicts with information of each book.
        """
        data_for_json = []
        page_number = 1
        while True:
            page = BooksPage(page_number)
            data = page.books_info()
            if data:
                data_for_json.extend(data)
                page_number += 1
            else:
                break
        return data_for_json

    def write_file(self):
        """Write books info in a json file books.json."""
        with open('books.json', 'w', encoding='UTF-8') as file:
            json.dump(self.books_info(), file, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    j = Json()
    j.write_file()
